//
//  FlickImageGalleryTests.swift
//  FlickImageGalleryTests
//
//  Created by Bartlomiej Zdybowicz on 21/02/2017.
//  Copyright © 2017 Bartłomiej Zdybowicz. All rights reserved.
//

import XCTest
@testable import FlickImageGallery

class GalleryViewModelTest: XCTestCase {

    var flickrFeedItem: FlickrFeedItem {
        var flickrItem = FlickrFeedItem()
        flickrItem.author = "author"
        flickrItem.title = "title"
        return flickrItem
    }

    var galleryViewModel: GalleryViewModel {
        var galleryViewModel = GalleryViewModel()
        galleryViewModel.author = "author"
        galleryViewModel.title = "title"
        return galleryViewModel
    }

    var galleryViewModel1: GalleryViewModel {
        var galleryViewModel = GalleryViewModel()
        galleryViewModel.author = "author"
        galleryViewModel.title = "title"
        galleryViewModel.imageURLString = "something"
        return galleryViewModel
    }

    var galleryViewModel2: GalleryViewModel {
        var galleryViewModel = GalleryViewModel()
        galleryViewModel.author = "author"
        return galleryViewModel
    }

    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
       super.tearDown()
    }
    

    func testInit_ProperFlickFeedItemPassed_InitializedValidGalleryViewModel() {
        let galleryModel = GalleryViewModel(flickrItem: flickrFeedItem)
        XCTAssertEqual(galleryModel, galleryViewModel)
    }

    func testInit_MissingURLFlickFeedItemPassed_InitializedInValidGalleryViewModel() {
        let galleryModel = GalleryViewModel(flickrItem: flickrFeedItem)
        XCTAssertFalse(galleryModel == galleryViewModel1)
    }

    func testInit_AdditionalTitleFlickFeedItemPassed_InitializedInValidGalleryViewModel() {
        let galleryModel = GalleryViewModel(flickrItem: flickrFeedItem)
        XCTAssertFalse(galleryModel == galleryViewModel2)
    }
}

private extension GalleryViewModel {

    init() {
        imageURLString = ""
        title = ""
        author = ""
        dateLabel = ""
        publishedDate = ""
    }
}
