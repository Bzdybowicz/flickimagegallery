//
//  GalleryDataSource.swift
//  FlickImageGallery
//
//  Created by Bartlomiej Zdybowicz on 21/02/2017.
//  Copyright © 2017 Bartłomiej Zdybowicz. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire

class GalleryDataSource: NSObject,
UICollectionViewDelegate,
UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: UICollectionView?

    let fixedSize: CGFloat = 300

    var viewModel: [GalleryViewModelProtocol]? {
        didSet {
            collectionView?.reloadData()
        }
    }

    let galleryCellId = "galleryCell"

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: fixedSize, height: fixedSize)
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: galleryCellId, for: indexPath) as? GalleryCell,
            let viewModel = viewModel else {
            return UICollectionViewCell()
        }

        let viewModelItem = viewModel[indexPath.row]

        cell.authorLabel.text = viewModelItem.author
        cell.dateLabel.text = viewModelItem.dateLabel
        cell.publishedDate.text = viewModelItem.publishedDate
        cell.titleLabel.text = viewModelItem.title
        if let url = viewModelItem.imageURL {
            cell.imageView.af_setImage(withURL: url)
        }

        return cell
    }
}
