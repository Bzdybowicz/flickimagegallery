//
//  ViewController.swift
//  FlickImageGallery
//
//  Created by Bartlomiej Zdybowicz on 21/02/2017.
//  Copyright © 2017 Bartłomiej Zdybowicz. All rights reserved.
//

import UIKit

class GalleryViewController: UIViewController {

    var sessionData: SessionData? {
        didSet {
            dataSource.viewModel = prepareViewModel(data: sessionData?.flickrFeed)
            sessionData?.registerForFeedUpdates(subscriber: self)
        }
    }

    @IBOutlet var dataSource: GalleryDataSource!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    func prepareViewModel(data: FlickrFeed?) -> [GalleryViewModelProtocol]? {
        guard let modelData = data else {
            return nil
        }

        var viewModel = [GalleryViewModelProtocol]()

        modelData.items?.forEach({
            viewModel.append(GalleryViewModel(flickrItem: $0))
        })

        return viewModel
    }
}

// MARK: - FeedSubscriberProtocol
extension GalleryViewController: FeedSubscriberProtocol {
    func feedWasUpdated(newData: FlickrFeed) {
        dataSource.viewModel = prepareViewModel(data: newData)
    }
}
