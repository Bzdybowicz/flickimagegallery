//
//  GalleryCell.swift
//  FlickImageGallery
//
//  Created by Bartlomiej Zdybowicz on 21/02/2017.
//  Copyright © 2017 Bartłomiej Zdybowicz. All rights reserved.
//

import UIKit

class GalleryCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var publishedDate: UILabel!
}
