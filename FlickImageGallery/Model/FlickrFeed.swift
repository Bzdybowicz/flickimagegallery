//
//  FlickrFeedObject.swift
//  FlickImageGallery
//
//  Created by Bartlomiej Zdybowicz on 21/02/2017.
//  Copyright © 2017 Bartłomiej Zdybowicz. All rights reserved.
//

import Foundation
import ObjectMapper

struct FlickrFeed {
    var title: String?

    var link: String?

    var description: String?

    var modified: String?

    var generator: String?

    var items: [FlickrFeedItem]?
}

extension FlickrFeed: Mappable {

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        title <- map["title"]
        link <- map["link"]
        description <- map["description"]
        modified <- map["modified"]
        generator <- map["generator"]
        items <- map["items"]
    }
}
