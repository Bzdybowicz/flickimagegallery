//
//  FlickFeedItem.swift
//  FlickImageGallery
//
//  Created by Bartlomiej Zdybowicz on 21/02/2017.
//  Copyright © 2017 Bartłomiej Zdybowicz. All rights reserved.
//

import Foundation
import ObjectMapper

struct FlickrFeedItem {

    var title: String?

    var link: String?

    var media: Dictionary<String, String>?

    var dataTaken: String?

    var description: String?

    var published: String?

    var author: String?

    var authorId: String?

    var tags: String?
}

extension FlickrFeedItem: Mappable {

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        title <- map["title"]
        link <- map["link"]
        media <- map["media"]
        dataTaken <- map["date_taken"]
        description <- map["description"]
        published <- map["published"]
        author <- map["author"]
        authorId <- map["authorId"]
        tags <- map["tags"]
    }
}

extension FlickrFeedItem: Equatable {}

func == (lhs: FlickrFeedItem, rhs: FlickrFeedItem) -> Bool {

    if (lhs.media == nil && rhs.media != nil) || (lhs.media != nil && rhs.media == nil) {
        return false
    }
    if let lhsMedia = lhs.media, let rhsMedia = rhs.media {
        let mediaEqual = NSDictionary(dictionary: lhsMedia).isEqual(to: rhsMedia)
        if mediaEqual == false {
            return false
        }
    }

    return lhs.title == rhs.title && lhs.link == rhs.link && lhs.dataTaken == rhs.dataTaken && lhs.description == rhs.description && lhs.published == rhs.published && lhs.author == rhs.author && lhs.authorId == rhs.authorId && lhs.tags == rhs.tags
}
