//
//  FeedSubscriberProtocol.swift
//  FlickImageGallery
//
//  Created by Bartlomiej Zdybowicz on 21/02/2017.
//  Copyright © 2017 Bartłomiej Zdybowicz. All rights reserved.
//

import Foundation

protocol FeedSubscriberProtocol {
    func feedWasUpdated(newData: FlickrFeed)
}
