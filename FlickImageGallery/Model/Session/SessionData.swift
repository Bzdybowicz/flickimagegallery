//
//  SessionData.swift
//  FlickImageGallery
//
//  Created by Bartlomiej Zdybowicz on 21/02/2017.
//  Copyright © 2017 Bartłomiej Zdybowicz. All rights reserved.
//

import Foundation

class SessionData {

    var feedSubscribers = [FeedSubscriberProtocol]()

    var updateInterval: TimeInterval?

    var flickrFeed: FlickrFeed? {
        didSet {
            guard let newFeed = flickrFeed else {
                return
            }
            feedSubscribers.forEach({ $0.feedWasUpdated(newData: newFeed) })
        }
    }

    func registerForFeedUpdates(subscriber: FeedSubscriberProtocol) {
        feedSubscribers.append(subscriber)
    }
}
