//
//  FlickAPIService.swift
//  FlickImageGallery
//
//  Created by Bartlomiej Zdybowicz on 21/02/2017.
//  Copyright © 2017 Bartłomiej Zdybowicz. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper


class FlickrAPIService {

    let galleryURLString = "https://api.flickr.com/services/feeds/photos_public.gne"
    let jsonParametersDictionary = ["format": "json"]

    func fetchFeedData(completion: @escaping (FlickrFeed?) -> ())  {
        Alamofire.request(galleryURLString, parameters: jsonParametersDictionary).responseString { (dataResponseString) in
            let stringResponse = dataResponseString.result.value
            /// [TODO] Investigate why API returns invalid JSON.
            /// Is there any missing parameter?
            let removeStartJSONStringError = stringResponse?.replacingOccurrences(of: "jsonFlickrFeed(", with: "")
            let parsableJSONString = removeStartJSONStringError?.replacingOccurrences(of: ")", with: "")
            guard let parsableString = parsableJSONString else {
                completion(nil)
                return
            }

            let mappedResponse = Mapper<FlickrFeed>().map(JSONString: parsableString)
            completion(mappedResponse)
        }
    }
}
