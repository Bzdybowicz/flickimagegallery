//
//  GalleryViewModell.swift
//  FlickImageGallery
//
//  Created by Bartlomiej Zdybowicz on 21/02/2017.
//  Copyright © 2017 Bartłomiej Zdybowicz. All rights reserved.
//

import Foundation

struct GalleryViewModel: GalleryViewModelProtocol {

    private let imageURLDictionaryKey = "m"

    var imageURLString: String

    var title: String

    var author: String

    var dateLabel: String

    var publishedDate: String

    init(flickrItem: FlickrFeedItem) {
        imageURLString = flickrItem.media?[imageURLDictionaryKey] ?? ""
        title = flickrItem.title ?? ""
        author = flickrItem.author ?? ""
        dateLabel = flickrItem.dataTaken ?? ""
        publishedDate = flickrItem.published ?? ""
    }

    var imageURL: URL? {
        return URL(string: imageURLString)
    }
}

extension GalleryViewModel: Equatable {}

func == (lhs: GalleryViewModel, rhs: GalleryViewModel) -> Bool {
    return lhs.author == rhs.author && lhs.title == rhs.title && lhs.imageURLString == rhs.imageURLString && lhs.dateLabel == rhs.dateLabel && lhs.publishedDate == rhs.publishedDate
}
