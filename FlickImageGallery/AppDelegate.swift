//
//  AppDelegate.swift
//  FlickImageGallery
//
//  Created by Bartlomiej Zdybowicz on 21/02/2017.
//  Copyright © 2017 Bartłomiej Zdybowicz. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    /// Flickr api service. Forced to exist - app has no point to run without it.
    var flickrService: FlickrAPIService!

    /// App session object - enough data storage to fill requirements. Forced to exist - app has no point to run without it.
    var sessionData: SessionData!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        startSession()
        prefetchData()
        let galleryViewController = ViewControllerFactory.galleryViewController(session: sessionData)

        self.window?.rootViewController = galleryViewController
        self.window?.makeKeyAndVisible()

        return true
    }

    func startSession() {
        sessionData = SessionData()
    }

    func prefetchData() {
        flickrService = FlickrAPIService()
        flickrService.fetchFeedData(completion: { (feedData) in
            self.sessionData?.flickrFeed = feedData
        })
    }
}
