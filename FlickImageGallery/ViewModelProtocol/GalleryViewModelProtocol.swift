//
//  GalleryViewModel.swift
//  FlickImageGallery
//
//  Created by Bartlomiej Zdybowicz on 21/02/2017.
//  Copyright © 2017 Bartłomiej Zdybowicz. All rights reserved.
//

import Foundation

protocol GalleryViewModelProtocol {

    var imageURLString: String {get set}

    var title: String {get set}

    var author: String {get set}

    var dateLabel: String {get set}

    var publishedDate: String {get set}

    var imageURL: URL? {get}
}
