//
//  ViewControllerFactory.swift
//  FlickImageGallery
//
//  Created by Bartlomiej Zdybowicz on 21/02/2017.
//  Copyright © 2017 Bartłomiej Zdybowicz. All rights reserved.
//

import UIKit

class ViewControllerFactory {

    static let galleryStoryboardId = "Gallery"

    class func galleryViewController(session: SessionData) -> GalleryViewController? {
        guard let galleryViewController = viewController(storyBoardName: galleryStoryboardId) as? GalleryViewController else {
            print("Cannot create gallery view controller!")
            return nil
        }
        galleryViewController.sessionData = session
        return galleryViewController
    }
}

private extension ViewControllerFactory {
    class func viewController(storyBoardName: String,
                              identifier: String? = nil,
                              bundle: Bundle? = nil) -> UIViewController? {
        let storyboard = UIStoryboard(name: storyBoardName, bundle: bundle)

        if let identifier = identifier {
            return storyboard.instantiateViewController(withIdentifier: identifier)
        } else {
            return storyboard.instantiateInitialViewController()
        }
    }
}
